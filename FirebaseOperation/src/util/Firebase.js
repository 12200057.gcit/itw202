import { firebase } from '@firebase/app'

const CONFIG = {
  apiKey: "AIzaSyB2uqkegb4iF2yp-JUt7Aweoj0fpLkpaJ0",
  authDomain: "fir-operation-c7e1f.firebaseapp.com",
  databaseURL: "https://fir-operation-c7e1f-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "fir-operation-c7e1f",
  storageBucket: "fir-operation-c7e1f.appspot.com",
  messagingSenderId: "732515525171",
  appId: "1:732515525171:web:7789b62f4fc16ad45f7946",
  measurementId: "G-W53YP2GQQN"
}
firebase.initializeApp(CONFIG)

export default firebase;
