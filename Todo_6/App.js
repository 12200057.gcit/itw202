import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.square1}><Text>1</Text></View>
      <View style={styles.square2}><Text>2</Text></View>
      <View style={styles.square3}><Text>3</Text></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:"row",
    padding:60,
    paddingRight:90
  },
  square1:{
    backgroundColor:'red',
    width:55,
    height:315,
    justifyContent:'center',
    alignItems:'center'
  },
  square2:{
    backgroundColor:'blue',
    width:160,
    height:315,
    justifyContent:'center',
    alignItems:'center'
  },
  square3:{
    backgroundColor:'green',
    width:10,
    height:315,
    justifyContent:'center',
    alignItems:'center'

  },
});
