import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View,ScrollView,Image,TextInput,Button } from 'react-native';
import ViewComponent from './components/ViewComponent';
import TextComponent from './components/TextComponent';
import ImageComponent from './components/ImageComponent';
import TextInputComponent from './components/TextInput Component';

export default function App() {
  return (
    <ScrollView style={{marginTop:30}} >
      <Text>Some text</Text>
      <View>
        <Text>Some more text</Text>
        <Image
        style={{width:51,
        height:51,}}
        source={{
          uri:'https://picsum.photos/64/64',
        }}/>
      </View>
      <TextInput
      defaultValue='You can type here'/>
      <Button
      onPress={()=>{
        alert('You tapped the button!');
      }}
      title='Press Me'/>
      <View style={{height:10}}></View>
          <ViewComponent></ViewComponent>
          <TextComponent></TextComponent>
          <ImageComponent></ImageComponent>
          <TextInputComponent/>

    </ScrollView>
    


   );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
   
  },
 
});
