import React from 'react'
import {View} from 'react-native'
import  HomeScreen  from '../src/Screens/HomeScreen';
import ProfileScreen from '../src/Screens/ProfileScreen';
import MyTab from './MyTab'
import { createDrawerNavigator } from '@react-navigation/drawer';
import 'react-native-gesture-handler'

const Drawer = createDrawerNavigator();

export default function MyDrawer() {

  return (
      <Drawer.Navigator >  
        <Drawer.Screen name='Home' component={MyTab}/>
        <Drawer.Screen name='Profile' component={ProfileScreen}/>
      </Drawer.Navigator>  
  )
}
