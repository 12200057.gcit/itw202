import React from 'react'
import {TouchableOpacity, Text, StyleSheet,View} from 'react-native';
import {COLORS} from '../constants/Colors';
import {RowItem} from './RowItems';
function CustomComponents() {
    return (
        <View style={styles.container}>
            <RowItem
                text ="Themes"
                />
            <RowItem
                text="React Native Basics"
            />
            <RowItem
                text="React Native by Example"
            />

            {/* <TouchableOpacity style={styles.row}>
                <Text style={styles.text}>Theme</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.row}>
                <Text>React Native Basics</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.row}>
                <Text>React Native by Example</Text>
            </TouchableOpacity> */}
        </View>
    )
}

export default CustomComponents

const styles=StyleSheet.create({
    container:{
        marginTop:30
    },
    row:{
        paddingHorizontal:20,
        paddingVertical:16,
        justifyContent:"space-between",
        alignItems:"center",
        flexDirection:"row",
        backgroundColor:COLORS.white,
    },
    title:{
        color:COLORS.text,
        fontSize:16,
    },
})