import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import ButtonExample from './components/ButtonExample';
import TouchableHighlightExample from './components/TouchableHighlightExample';
import TouchableNativeFeedbackExample from './components/TouchableNativeFeedbackExample';
import TouchableOpacityExample from './components/TouchableOpacityExample';
import TouchableWithoutFeedbackExample from './components/TouchableWithoutFeedbackExample';
import CustomComponents from './components/CustomComponents';
export default function App() {
  return (
    <View style={styles.container}>
      <ButtonExample></ButtonExample>
      <TouchableHighlightExample/>
      <TouchableNativeFeedbackExample/>
      <TouchableOpacityExample/>
      <TouchableWithoutFeedbackExample/>
      <CustomComponents/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
