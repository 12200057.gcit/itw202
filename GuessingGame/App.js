import { StyleSheet, SafeAreaView,ImageBackground } from 'react-native';
import StartGameScreen from './Screens/StartGameScreen';
import { LinearGradient } from 'expo-linear-gradient';
import GameScreen from './Screens/GameScreen';
import React, {useState} from 'react'
import { useFonts } from 'expo-font';
import Colors from './constants/colors';
import GameOverScreen from './Screens/GameOverScreen';
import AppLoading from 'expo-app-loading'
import { StatusBar } from 'expo-status-bar';


export default function App() {
  const [userNumber, setUserNumber] = useState();
  const [gameIsOver, setGameIsOver] = useState(true);
  const [guessRounds, setGuessRounds] = useState(0)

  const [fontsloaded] = useFonts({
    "open-sans": require('./assets/Fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/Fonts/OpenSans-Bold.ttf'),
  })

  if (!fontsloaded) {
    return <AppLoading/>
  }

  function pickedNumberHandler(pickerNumber){
    setUserNumber(pickerNumber);
    setGameIsOver(false);
  }

  function gameOverHandler(numberOfRounds){
    setGameIsOver(true);
    setGuessRounds(numberOfRounds)
  }

  function startNewGameHandler(){
    setUserNumber(null);
    setGuessRounds(0);

  }

  let screen = <StartGameScreen onPickNumber={pickedNumberHandler}/>
  if (userNumber){
    screen = <GameScreen userNumber={userNumber} onGameOver= {gameOverHandler} />
  }
  if(gameIsOver && userNumber){
    screen = <GameOverScreen
                  userNumber={userNumber}
                  roundsNumber={guessRounds}
                  onStartNewGame={startNewGameHandler}
    />
  }
  return (
    <>
    <StatusBar style='dark'/>
    <LinearGradient colors={[Colors.primary700,  Colors.accent500]} style={styles.container}>
      <ImageBackground
        source={require('../assets/Image/background.png')}
        resizeMode="cover"
        style={styles.rootScreen}
        imageStyle={styles.backgroundImage}
        ></ImageBackground>
      
      <SafeAreaView style={styles.container}>
        {screen}
      </SafeAreaView>
    </LinearGradient>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    //backgroundColor: '#ddb52f',
    flex: 1,
  },
  rootScreen:{
    flex:1
  },
  backgroundImage:{
    opacity:0.15
  }
 
});
