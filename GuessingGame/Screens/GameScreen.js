import { StyleSheet, Text, View,Alert,FlatList,useWindowDimensions} from 'react-native'
import React, {useState, useEffect} from 'react'
import Title from '../components/ui/title'
import NumberContainer from '../components/game/NumberContainer';
import Primarybutton from '../components/ui/Primarybutton';
import Card from '../components/ui/Card';
import InstructionText from '../components/ui/InstructionText';
import {Ionicons} from '@expo/vector-icons'
import GuessLogItem from '../components/game/GuessLogItem';



function generateRandomBetween(min, max,exclude) {
  const rndNum = Math.floor(Math.random() * (max-min)) + min;

  if (rndNum == exclude){
    return generateRandomBetween(min, max, exclude);
  }
  else {
    return rndNum;
  }
}

let minBoundry = 1;
let maxBoundry = 100;

function GameScreen({userNumber,onGameOver}) {

  const initialGuess = generateRandomBetween(1, 100, userNumber)
  const [currentGuess, setCurrentGuess] = useState(initialGuess)
  const [guessRounds, setGuessRounds] = useState([initialGuess]);
  const {width,height} = useWindowDimensions();

  useEffect(()=>{
    if (currentGuess === userNumber){
      onGameOver(guessRounds.length);
    }
  },[currentGuess, userNumber, onGameOver])

  useEffect(() =>{
    minBoundry = 1;
    maxBoundry = 100;
  },[])


  function nextGuessHandler(direction){
    if(
      (direction === 'lower' && currentGuess < userNumber) ||
      (direction === 'greater' && currentGuess > userNumber))
    {
      Alert.alert("Don't lie!", 'you know that this wrong...',[
        {text: 'Sorry', style: 'cancel'}
      ])
      return;
    }

    if(direction === 'lower'){
      maxBoundry = currentGuess;
    }
    else{
      minBoundry = currentGuess + 1;
    }
    console.log(minBoundry,maxBoundry)
    const newRndNumber = generateRandomBetween(minBoundry, maxBoundry,currentGuess)

    setCurrentGuess(newRndNumber);
    setGuessRounds((prevGuessRounds => [newRndNumber, ...prevGuessRounds]))
  }
  const guessRoundsListLength = guessRounds.length

  let content = (
  <>
     <NumberContainer>{currentGuess}</NumberContainer>
     <Card>
        <InstructionText style={styles.InstructionText}>Higher or lower?</InstructionText>
        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
          <Primarybutton onPress={nextGuessHandler.bind(this,'greater')}>
            <Ionicons name='md-add' size={24} color='white'/>
          </Primarybutton>
          </View>
          <View style={styles.buttonContainer}>
            <Primarybutton onPress={nextGuessHandler.bind(this, 'lower')}>
              <Ionicons name='md-remove' size={24} color='white'/>
            </Primarybutton>
          </View>
        </View>
      </Card>
  </>
  );

  if(width>500){
    content=(
      <>
        {/* <InstructionText style={styles.InstructionText}>Higher or lower?</InstructionText> */}
       <View style={styles.buttonContainerWide}>
          <View style={styles.buttonContainer}>
          <Primarybutton onPress={nextGuessHandler.bind(this,'greater')}>
            <Ionicons name='md-add' size={24} color='white'/>
          </Primarybutton>
          </View>

          <NumberContainer>{currentGuess}</NumberContainer>
          <View style={styles.buttonContainer}>
            <Primarybutton onPress={nextGuessHandler.bind(this, 'lower')}>
              <Ionicons name='md-remove' size={24} color='white'/>
            </Primarybutton>
          </View>
        </View>
       
      </>
    )
  }


  return (
    <View style={styles.screen}>
      <Title>Opponent's Guess</Title>
      {content}
      <View style={styles.listContainer}>
        <FlatList
        data={guessRounds}
        renderItem = {(itemData) =>
          <GuessLogItem
          roundNumber={guessRoundsListLength - itemData.index}
          guess = {itemData.item}
          />
        }
        keyExtractor = {(item) => item}
        />
      </View>
    </View>
  )
}

export default GameScreen

const styles = StyleSheet.create({
  listContainer: {
    flex: 1,
    padding: 12
  },

  InstructionText: {
    marginBottom: 12
  },
  screen: {
    flex: 1,
    padding: 24,
    alignItems:'center'
  },
  
  buttonsContainer: {
    flexDirection: 'row'
  },
  buttonContainer: {
    flex: 1,
  },
  buttonContainerWide:{
    flexDirection:'row',
    alignItems:'center'
  }
})