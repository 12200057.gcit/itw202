import { StyleSheet, Text, View,Image,Dimensions,useWindowDimensions,ScrollView } from 'react-native'
import React from 'react'
import Title from '../components/ui/title'
import { Colors } from 'react-native/Libraries/NewAppScreen'
import Primarybutton from '../components/ui/Primarybutton'

function GameOverScreen({roundsNumber,userNumber, onStartNewGame}){
  const {width,height} = useWindowDimensions();

  let imageSize = 300;
  if(width<380){
    imageSize= 150;
  }

  if (height<400){
    imageSize=80;
  }
  const imageStyle ={
    width:imageSize,
    height:imageSize,
    borderRadius:imageSize / 2
  }
  return (
  <ScrollView style={styles.screen}>
    <View style={styles.rootContainer}>
      <Title>GAME OVER!</Title>
      <View style={[styles.imagecontainer,imageStyle]}>
        <Image
          style={styles.image} 
          source={require('../assets/Image/success.png')}/>
      </View>
      <Text style={styles.summaryText}>
        Your phone needed <Text style={styles.highlight}>{roundsNumber} </Text>
        rounds to guess the number <Text style={styles.highlight}> {userNumber}</Text>
      </Text>
      <Primarybutton onPress={onStartNewGame}>Start New Game</Primarybutton>
    </View>
    </ScrollView>
    
  )
}

export default GameOverScreen
const deviceWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
  Screen:{
    flex:1
  },
  rootContainer: {
    flex: 1,
    padding: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imagecontainer: {
    width: deviceWidth < 380 ? 150: 300,
    height: deviceWidth < 380 ? 150: 300,
    borderRadius: deviceWidth < 380 ? 75: 150,
    borderWidth: 3,
    borderColor: Colors.primary800,
    overflow: 'hidden',
    margin: 36,
  },
  image: {
    height: '100%',
    width: '100%',
  },
  summaryText: {
    fontFamily: 'open-sans',
    fontSize: 24,
    alignItems: 'center',
    marginBottom: 24
  },
  highlight: {
    fontFamily: 'open-sans-bold',
    color: Colors.primary500
  }
})