import { Text, View, Pressable, StyleSheet } from 'react-native'
import React from 'react'
import Colors from '../../constants/colors';

function Primarybutton({ children, onPress}) {
    return (
        <View style={styles.buttonOuterContainer}>
            <Pressable android_ripple={{color: Colors.primary600}}
                 onPress={onPress}
                 style={({pressed}) => pressed ? [styles.buttonInnerContainer,styles.pressed]: styles.buttonInnerContainer}
            >
                <Text style={styles.buttonText}>{children}</Text>
            </Pressable>
        </View>
    );
}

export default Primarybutton;

const styles = StyleSheet.create({
       buttonOuterContainer: {
        backgroundColor: '#72063c',
        borderRadius: 28,
        paddingVertical: 8,
        paddingHorizontal: 16,
        elevation: 2,
        margin: 4,
    },
    buttonInnerContainer: {
        backgroundColor: Colors.primary500,
        borderRadius: 28,
        paddingVertical: 8,
        paddingHorizontal: 16,
        elevation: 2,
        margin: 4,

    },
    buttonText: {
        color: 'white',
        textAlign: 'center'
    },
    pressed: {
        opacity: 0.75,
    }

});