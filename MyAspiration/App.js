import React, {useState} from 'react';
import { Button, StyleSheet, Text, TextInput, View,ScrollView, FlatList } from 'react-native';

import { AspirationItem } from './components/AspirationItem';
import AspirationInput from './components/AspirationInput'

export default function App() {

  // const [enteredAspiration,setEnteredAspiration]=useState('');
  const [courseApirations, setCourseAspirations]=useState([]);

  // const AspirationInputHandler = (enteredText) => {
  //   setEnteredAspiration(enteredText);
  // };

  const addAspirationHandler = aspirationTitle=> {
    // console.log(enteredAspiration);
    // setCourseAspirations([...courseApirations,enteredAspiration]);
    setCourseAspirations(currentAspiration => [
      ...courseApirations,
      {key: Math.random().toString(), value: aspirationTitle}
    ])
  };

  return (
    <View style={styles.screen}>
      <AspirationInput onAddAspiration = {addAspirationHandler}/>
      {/* <View style={styles.inputContainer}>
        <TextInput
          placeholder="My Aspiration from this module" 
          style={styles.input}
          onChangeText={AspirationInputHandler}
          value={enteredAspiration}
        />
        <Button
          title='ADD'
          onPress={addAspirationHandler}

        />
        </View> */}
      
      {/* <ScrollView>
        {courseApirations.map((aspiration)=>
        <View key={aspiration} style={styles.listItem}>
        <Text >{aspiration}</Text>
        </View>
        )}
        </ScrollView> */}
        <FlatList
        data={courseApirations}
        renderItem ={itemData => 
          <AspirationItem title={itemData.item.value}/>
          
        }
        />
      
    </View>
  );
}

const styles = StyleSheet.create({
 screen: {
    padding: 50
  },
  inputContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
  },
  input:{
    width:'80%',
    borderColor:'black',
    borderWidth:1,
    padding:10
  },
  listItem:{
    padding:10,
    marginVertical:10,
    backgroundColor:'#ccc',
    borderColor:'black',
    borderWidth:1
  }
});
