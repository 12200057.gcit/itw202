import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
// import {add, multiply } from './component/nameExport';
// import division from './component/defaultExport';
import Courses from './component/funcComponent';
export default function App() {
  return (
    <View style={styles.container}>
      {/* <Text> Open up App.js to start working on your app!</Text>
      <Text>Result of Addition: {add(5,6)}</Text>
      <Text>Result of Multiplication:{multiply (5,6)}</Text>
      <Text>Result of division: {division(10,2)}</Text> */}
      <Courses></Courses>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
