import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.square1}></View>
      <View style={styles.square2}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop:350,
  },
  square1:{
    backgroundColor:'red',
    borderWidth:1,
    borderColor:'red',
    height:140,
    width:140,
  },
  square2:{
    backgroundColor:'blue',
    borderWidth:1,
    borderColor:'blue',
    height:140,
    width:140,
  }
});
