import React from 'react';
import {StyleSheet,Text,View} from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor:'#F5FCFF'
    },
    Welcome:{
        fontSize:20,
        textAlign:'center',
        margin:10
    }
});
export default styles;