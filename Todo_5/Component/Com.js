import React from 'react';
import {StyleSheet,View,Text} from "react-native";

const Com=()=>{
    const greeting = 'Getting started\nwith react\nnative!'
    const name ='My name is Kinley\nChoden';
    return (
        <View>
            <Text style={style.textStyle}>{greeting}</Text>
            <Text style={{fontSize:20,textAlign:'center',paddingRight:100,fontWeight:'bold'}}>{name}</Text>
        </View>

    )
};
const style =StyleSheet.create({
    textStyle:{
        fontSize:45,
        textAlign:'center',
        paddingRight:60,
    }
});
export default Com;